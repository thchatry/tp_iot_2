import paho.mqtt.client as mqtt
import json
import base64
import codecs 

from Crypto.Cipher import AES

#fonction Xor bit à bit sous python
def byte_xor(ba1, ba2): 
    return bytes([_a ^ _b for _a, _b in zip(ba1, ba2)])

# Callback déclenchée lorsque vous vous connectez au broker
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # S'abonner au sujet "isima/tiki/up"
    client.subscribe("isima/tiki/up")

# Callback déclenchée lorsque vous recevez un message du sujet
def on_message(client, userdata, msg):
    print("\n  ------- Message reçu ------- \n")

    print("Message received on topic: "+msg.topic)

    m_decode=str(msg.payload.decode("utf-8","ignore"))
    print("\ndata Received : {}".format(m_decode)) #affichage des donnees entrees
    m_in=json.loads(m_decode) #decode json data
    
    payload = m_in["payload"] 
    print("payload decode b64 value : {}".format(int.from_bytes(base64.b64decode(payload),"big")))
    
    valueDecode = base64.b64decode(payload) #valeur recu par la carte (crypté)
    print("valueDecode = {}".format(valueDecode))

    cptCrypted = CryptoCpt() #calcul du Cpt crypté
    
    result = byte_xor(valueDecode,cptCrypted) #décryptage avec le XOR entre la valeur recu et le Cpt 

    print("\n ---- resultat : {} ------ \n".format(int.from_bytes(result,"big"))) #affichage du résultat 

# fonction pour calculer le Cpt 
def CryptoCpt():
    print("\n ------ Calcul du Cpt crypté ---------\n")

    key = '000102030405060708090A0B0C0D0E0F' #clef en string
    cpt = '00000000000000000000000000000000' #cpt en string
    
    keyHexa = codecs.decode(key,'hex_codec') #clef en hexa
    print("clef en hexa(byte) : {}".format(keyHexa))

    cptHexa = codecs.decode(cpt,'hex_codec') #cpt en hexa
    print("compteur en hexa(byte) : {}".format(cptHexa))

    cipher = AES.new(keyHexa, AES.MODE_ECB) #cryptage AES
    cptCrypted = cipher.encrypt(cptHexa)


    print("\nCpt crypté : {} ".format(cptCrypted))
    print("Cpt crypted in hex = {}".format(cptCrypted.hex()))
    print("\n ------------------------------- \n")

    return cptCrypted 


########  Début du script : ##########

# Créer une instance du client MQTT
client = mqtt.Client()

# Définir les callbacks
client.on_connect = on_connect
client.on_message = on_message

# Se connecter au broker
client.connect("198.27.70.149", 443, 60)
# Boucle de traitement des messages
client.loop_forever()

########   Fin du script :  ##########
