# TP_IOT_2

Here is the result of the TP2, carried out as part of the "Connected objects" course, supervised by Paul PINOT. 

## Getting started

This project consists of three part:
- the .ino: This is the code for the Arduino board. With this code, the card will send on the Helium network a message encrypted with the AES-CTR protocol every 30 seconds.
- We have configured the device on the helium network so that the data is returned to an MQTT server.
- The python script subscribes to the MQTT server, decrypts the message and displays it.

## Installation

To use the python script, the pycryptodome and paho-mqtt library are required.
```bash
pip install pycryptodome
pip install paho-mqtt
```
## Launch

To laucnh, run the commande:
```python
 python .\sub.py
```
No other arguments needed. The key and the adresse of the MQTT server are written hard in the code. The script does not take into account the incrementation of the counter, and considers that it is always equal to 0.

## Modifications
This project is based on the correction of the TP1. All our modifications are framed by the tags:

```C
/***********************/
    ...
/***********************/
```


First addition: Define the key and the counter.
```C
AES128 aes128;
uint8_t key[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
uint8_t cpt[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
```

Second addition: Set the key.
```C
void setup{
    ...
    aes128.setKey(key, 16);
}
```

Last addition: Encrypte the data (*frameCpt*) before sending.
```C
    aes128.encryptBlock(encrypted_cpt,cpt);
    encrypted_frame = frameCpt ^ encrypted_cpt[0];
    
    // Incremente the cpt (not yet implemented in the receiver)
    cpt[15]++
```


## Authors

Kilian Zehnder, Thibault Chatry

